  def car_use
    show_one

     respond_to do |format|
      format.html
      format.pdf do

        # Thin ReportsでPDFを作成
        # 先ほどEditorで作ったtlfファイルを読み込む
        report = ThinReports::Report.new :layout => "app/pdfs/order_pdf.tlf"

          # 1ページ目を開始
        report.start_new_page
         link_date = Gw.datetime_to_date(@item.st_at)

        # 開始日時、終了日時の文字列
        time_show = @item.time_show

        today = Date.today.to_time
        st_date = "#{I18n.l @item.st_at.to_date, format: :date5}"
        ed_date = "#{I18n.l @item.ed_at.to_date, format: :date5}"
        visit = @item.title
        place = @item.place

        report.page.item(:today).value(today)
        report.page.item(:st_day).value(st_date)
        report.page.item(:ed_day).value(ed_date)
        report.page.item(:visit).value(visit)
        report.page.item(:place).value(place)

         @item.schedule_users.each do |user|
           user = System::User.where("id=#{user.uid}").first
           report.list(:user).add_row do |row|
             row.values username: user.name
           end
         end

        # ブラウザでPDFを表示する
        # disposition: "inline" によりダウンロードではなく表示させている
                send_data report.generate, filename: '社有車使用届.pdf',
                                           type: 'application/pdf',
                                           disposition: 'inline'
     end

    end

  end